angular.module('myApp',['ionic']).config(['$ionicConfigProvider', function($ionicConfigProvider) {
    $ionicConfigProvider.platform.ios.tabs.style('standard');
    $ionicConfigProvider.platform.ios.tabs.position('bottom');
    $ionicConfigProvider.platform.android.tabs.style('standard');
    $ionicConfigProvider.platform.android.tabs.position('bottom');
    $ionicConfigProvider.platform.ios.navBar.alignTitle('left');
    $ionicConfigProvider.platform.android.navBar.alignTitle('left');
    $ionicConfigProvider.platform.ios.backButton.previousTitleText('').icon('ion-ios-arrow-thin-left');
    $ionicConfigProvider.platform.android.backButton.previousTitleText('').icon('ion-android-arrow-back');
    $ionicConfigProvider.platform.ios.views.transition('ios');
    $ionicConfigProvider.platform.android.views.transition('android');
}])

.controller('RootCtrl', function($scope) {
  $scope.$on('WantChat', function(e, item) {
    $scope.$broadcast('ChatWith', item);
  });
})

.controller('DialogCtrl', function($scope) {
  $scope.value = 'someone';

  $scope.$on('ChatWith', function(e, item) {
    $scope.value = item.title;
  });

  $scope.backClick = function() {
    $(".chat-dialog").hide();
    $(".main-page").show();
  };

  $scope.AddPic = function() {
      console.log('addpic');
    navigator.camera.getPicture(function(imageData) {
        $("#captured-image-placeholder").attr("src", "data:image/jpeg;base64," + imageData);
        $(".picture-input-area").show();
    }, function(message) {
        console.log('Failed because: ' + message);
    }, {quality: 50,
        destinationType: navigator.camera.DestinationType.DATA_URL
    });
  };

  $scope.SendOut = function() {
      console.log('sendout');
    var imageURI = $("#captured-image-placeholder").attr("src");
    var text = $(".text-input-area textarea").val();
    var html = '<div class="one-message">' +
                 '<div class="text-message"><pre>' + text + '</pre></div>' +
                 '<div class="picture-message">' +
                   '<img src="' + (imageURI || "") + '">' +
                 '</div>' +
               '</div>';
    var $message = $(html);
    $(".message-display-area .messages").append($message);

    $.ajax({
        url: 'http://wty715.xicp.net:8000/',
        headers: {
            'Content-Type': 'application/json'
        },
        type: 'POST',
        data: JSON.stringify({
            origin: 2,
            destination: 1,
            systemCode: 0,
            content: text,
            picture: imageURI
        }),
        success: function(object) {
            console.log(object.systemCode);
        },
        error: function(message) {
            console.log('Error!');
            console.log(message);
        }
    });

    // 清空输入框和图片
    $(".text-input-area textarea").val("");
    $("#captured-image-placeholder").attr("src", "");
    $(".picture-input-area").hide();
  };

  $scope.DelPic = function() {
      console.log('delpic');
    var confirmation = confirm("删除图片吗？");
    if (!confirmation) return;
    
    // 清空并隐藏图片显示区
    $("#captured-image-placeholder").attr("src", "");
    $(".picture-input-area").hide();
  };    
})

.controller('UChatCtrl', function($scope, $timeout) {
  $scope.onRefresh = function() {
    $.ajax({
        url: 'http://wty715.xicp.net:8000/',
        headers: {
            'Content-Type': 'application/json',
            'pid': '1',
        },
        type: 'GET',
        success: function(object) {
            console.log(object.systemCode);
            if(object.systemCode == 0) {
                var html = '<div class="one-message">' +
                             '<div class="text-message"><pre>' + object.content + '</pre></div>' +
                             '<div class="picture-message">' +
                               '<img src="' + (object.picture || "") + '">' +
                             '</div>' +
                           '</div>';
                var $message = $(html);
                $(".message-display-area .messages").append($message);
            }
        },
        error: function(message) {
            console.log('Error!');
            console.log(message);
        }
    });

    $timeout(function() {
      $scope.$broadcast('scroll.refreshComplete');
    }, 1000);
  };

  $scope.items = [];

  $scope.$on('ChatWith', function(e, item) {
      var index = -1;
      for(var i=0; i<$scope.items.length; i++) {
          if($scope.items[i] == item) {
              index = i;
              break;
          }
      }
      if(index != -1) {
          $scope.items.splice(index,1);
      }
      $scope.items.unshift(item);
  });

  $scope.PinTop = function(item) {
      var index = -1;
      for(var i=0; i<$scope.items.length; i++) {
          if($scope.items[i] == item) {
              index = i;
              break;
          }
      }
      if(index != -1) {
          $scope.items.splice(index,1);
      }
      $scope.items.unshift(item);
  };

  $scope.onClick = function(item) {
      $(".main-page").hide();
      $scope.$emit('WantChat', item);
      $(".chat-dialog").show();
  };

  $scope.DelSession = function(item) {
      var index = -1;
      for(var i=0; i<$scope.items.length; i++) {
          if($scope.items[i] == item) {
              index = i;
              break;
          }
      }
      if(index != -1) {
          $scope.items.splice(index,1);
      }
  };
})

.controller('ContactCtrl', function($scope) {
  $scope.items = [{
      imgsrc: 'img/adele.jpeg',
      title: 'J金菊',
      description: '哲学复兴，势不可挡'
  },{
      imgsrc: 'img/emperor.jpeg',
      title: 'Z张至诚',
      description: '写Node.js好累'
  },{
      imgsrc: 'img/jason.jpeg',
      title: 'D大智',
      description: '学习好累'
  },{
      imgsrc: 'img/jun.jpeg',
      title: 'Y苑屹',
      description: '干什么都好累'
  },{
      imgsrc: 'img/justin.jpeg',
      title: 'J蒋吉',
      description: '泡妹子好累'
  },{
      imgsrc: 'img/ketty.jpeg',
      title: 'L李中华',
      description: '我也好累'
  },{
      imgsrc: 'img/King.jpeg',
      title: 'H胡运州',
      description: '开公司好累'
  },{
      imgsrc: 'img/lady.jpeg',
      title: 'Z周冠融',
      description: '代练好累'
  },{
      imgsrc: 'img/lenka.jpeg',
      title: 'Q寝室长',
      description: 'HTML5好累'
  },{
      imgsrc: 'img/master.jpeg',
      title: 'J囧小布',
      description: '互联网大赛好累'
  },{
      imgsrc: 'img/michael.jpeg',
      title: 'M迈克尔',
      description: '挂掉了好累'
  },{
      imgsrc: 'img/obama.jpeg',
      title: 'A奥巴马',
      description: '管美国好累'
  },{
      imgsrc: 'img/rihanna.jpeg',
      title: 'Z张潇叶',
      description: '跟雷总在一个城市好累'
  },{
      imgsrc: 'img/taylor.jpeg',
      title: 'C曹思远',
      description: '当个bitch好累'
  }];

  $scope.onClick = function(item) {
      $(".main-page").hide();
      $scope.$emit('WantChat', item);
      $(".chat-dialog").show();
  };
})

.controller('SettingsCtrl', function($scope) {

});