
#<center><font size="6px">第8组UChat开发文档</center>
#<font size="3px">小组成员：王天雨 张至诚 金菊
#<font size="3px">日期：2015.08.06
___

>1项目介绍 

1.1 **项目背景**  
&#8195;在HTML5实习结束的时候，需要根据课上所学习的内容进行融会贯通，进行软件的开发。  
&#8195;软件的要求是完成一个聊天软件的开发，要求界面尽可能的与 **微信** 相似。  
1.2 **基本特性**  
&#8195;要求实现的基本框架：  
&#8195;（1分）Header：顶部顶部Header包含左侧的"后退"按钮和标题，右侧的功能键，根据具体的界面会有所不同；  
&#8195;（1分）Navigation Bar：底部的导航栏包括"聊天"、"通讯录"和"设置"按钮，点击每个按钮可以切换到不同的界面；  
&#8195;（1分）Friend （Teammate） List View：通讯录列表界面应显示队友姓名和头像，按照字母排序，根据首字母分割，但不需要首字母索引跳转功能；点击队友姓名发起对话，并转到具体的聊天界面；  
&#8195;（3分）Chat List View：聊天列表界面显示所有发起的对话，点击可以进入具体的聊天界面，界面和功能参考微信；  
&#8195;（3分）Chat Details View：聊天详情界面能够正确显示自己和对方发出的内容，在底部提供消息输入框、"发送"按钮和其他功能按钮；  
&#8195;（1分）Pin Chat to Top：对话置顶功能有两处：1. 点击对话详情界面顶部右侧的功能按钮，将对话置顶；2. 在对话列表界面长按某一对话，在弹出的对话框中选择置顶对话；  
&#8195;（1分）Send Text：发送文本；  
&#8195;（1分）Send Pictures in Gallery：发送图库里的图片，UI和功能参考微信；  
&#8195;（1分）Send Pictures via Camera：发送摄像头拍的照片，UI和功能参考微信；  
&#8195;（1分）Send Emoticons：发送表情，消息输入框中可以使用文字方式显示表情，例如:坏笑: :大哭:，但是聊天详情中的对话应能够正确显示表情；  
&#8195; **期望结果：基本特性不需要实现服务端功能，所有聊天都是离线的，聊天消息是假的。**  
1.3 **高级特性**  
&#8195;要求实现一对一的实时聊天：  
&#8195;（2分）User Login and Logout：实现登陆和退出功能，不需要注册功能；每个人应该能够使用自己的账号和密码登录，服务端应该在账号&密码不匹配的情况下做出提示；通过导航栏的"设置"按钮进入设置界面，点击"退出"按钮退出登陆；  
&#8195;（2分）Send/Receive Text：实现文本的发送和接收功能；  
&#8195;（3分）Send/Receive Picture：实现图片的发送和接收功能；  
&#8195;（2分）Send/Receive Emoticon：实现表情的发送和接受功能；  
&#8195;（3分）Display Unread Messages：聊天列表界面显示每个对话的未读消息数量，查看后未读消息提示消失；  
&#8195;（3分）Receive Offline Messages：用户在上线后能够收到错过的离线消息，不需要实现push notification功能（即离线状态下依然接收消息）；  
&#8195; **期望结果：两个人可以线上一对一的聊天，参考微信。**  
1.3 **个人挑战**  
&#8195;要求让软件（UChat）更加像微信：  
&#8195;（20分）Start a Group Chat (with a least 3 persons)：能够创建并进行3人或以上的群聊；  
&#8195;（20分）Record / Send / Receive Micro Video (< 10 sec)：能够记录、发送、接收和播放微视频（<10秒）；  
&#8195;（20分）Record / Send / Receive Voice Message：能够记录、发送、接收和播放语音消息；  
&#8195;（20分）Join a Chat with QRCode：扫描二维码加入聊天；  
&#8195;（20分）Publish UChat for Off-campus Access (Publish app to a local store, and deploy backend to off-campus server, e.g. Heroku)：将UChat的手机应用发布到国内的应用商店上，并将服务端代码部署到非校园（公网）服务器上，例如Heroku
个人挑战的特性对UI界面没有具体要求，最主要的是实现功能，根据实现的质量、功能的完整性等得分。  
___  

>2项目开发环境  

&#8195;软件环境：Windows8 ; Intel XDK 2248; jQuery ; iOnic ; Node.js

&#8195;硬件环境：HTC Droid Incredible.

>3项目整体架构

3.1 **用户界面**  
&#8195;用户见面如下图所示，分为 **顶栏，内容和标签栏**。  
&#8195;点击标签栏中的三个按钮UChat、Contacts和Settings可以切换不同的内容。

![](http://a1.qpic.cn/psb?/20bf1293-85dd-4370-8cbc-e97016e2eb3f/AHgh5iDaO2mfhyt4I5SZ7S*sKgVDetHbif3uC8adVo8!/b/dBUAAAAAAAAA&bo=CQGpAQAAAAADAIU!&rf=viewer_4)
![](http://a3.qpic.cn/psb?/20bf1293-85dd-4370-8cbc-e97016e2eb3f/8yNxoBDIAjJPH77VV4fnS.rV4p*kBv9unjui8P2VwYs!/b/dAsAAAAAAAAA&bo=CAGoAQAAAAADAIU!&rf=viewer_4)
![](http://a3.qpic.cn/psb?/20bf1293-85dd-4370-8cbc-e97016e2eb3f/.dLebQzsfubLrWNGxw57tDn05sd2nyvl96v5RqCy7wg!/b/dBQAAAAAAAAA&bo=BwGqAQAAAAADB48!&rf=viewer_4)  
&#8195;  
3.2 **功能实现**  
&#8195;我们小组实现了以下功能：    
&#8195;1、在聊天（UChat）界面下 **下拉刷新**可以检查一次是否有新消息；  
&#8195;2、在聊天（UChat）界面下对每个联系人 **左滑**可以实现两项功能：a、点击绿色的Top按钮可以将其置顶；b、点击红色的按钮Del可以删除聊天；  
&#8195;3、点击右上角的 **“+Group”**按钮可以建立多人群聊；  
&#8195;4、在联系人（Contact）界面可以查看所有 **好友**；  
&#8195;5、在设置（Settings）界面可以做两件事：a、查看手机中所有的相片；b、登出当前的用户。

___
>4、API接口和通信协议

&#8195;客户端和服务器采用HTTP协议进行通信。形式有两种。

&#8195;1. 客户端GET（在HTTP Header），服务端回送一个json格式的messageItem对象。  
&#8195;客户端用GET方式访问localhost:8000，HTTP头部如下：
"Content-Type": "text/json"
"pid": "1"
若客户端未登陆，服务端回送1201。若该用户的消息队列为空，服务端回送1601。否则，服务端返回用户的消息队列的队头消息。

&#8195;2. 客户端POST一个json格式的messageItem对象，服务端回送一个json格式的messageItem对象
客户端可以通过POST向服务端提交数据来完成注册、登陆、获取信息、好友操作和群操作。具体格式见message.js（下文）
		
		exports.messageItem = function () {
			this.origin = 0;
			this.destination = 0;
		/*
			属性systemCode在非0时的含义如下：
			
					【由服务端发出】
					1101 - 注册成功。
					1102 - 注册失败（loginId已存在）。
					1103 - 注册失败（password为空）。
					1201 - 需要登陆。
					1202 - 登陆失败（loginId不存在）。
					1203 - 登陆失败（password错误）。
					1204 - 注销成功
					1205 - 注销失败（pid不存在）
					1301 - 修改自身密码成功。
					1302 - 修改自身密码失败（原password错误）。
					1303 - 修改自身密码失败（新password为空）。
					1304 - 获取他人信息失败（loginId不存在）。
					1305 - 获取他人信息失败（pid不存在）。
					1401 - 获取群信息失败（gid不存在）。
					1501 - 发送消息成功。
					1502 - 发送消息失败（对方非好友）。
					1503 - 发送消息失败（不在群中）。
					1601 - 消息队列为空。
					1701 - 转发成功。
					1702 - 转发失败（pid不存在）。
					1801 - 创建群失败（member为空）。
					1802 - 退出群成功。
					1803 - 退出群失败（不在群中）
					1804 - 退出群失败（gid不存在）。
					1999 - 报文格式错误。

				【客户端发出】（某些属性必须按需定义）.
					2101 - 注册.
						myLoginId
						myPassword
						myName
						myAvatar	
					2201 - 登陆。若服务端返回自身的personItem，则登陆成功.
						myLoginId
						myPassword
					2202 - 注销.
					2301 - 获取自身信息.
					2302 - 修改自身信息。若服务端返回自身的personItem，则修改自身信息成功.
						myName
						myAvatar
					2303 - 修改自身密码.
						myPassword
						myNewPassword
					2304 - 获取他人信息。若服务端返回他人的personItem，则获取他人信息成功.
						peerLoginId
						peerPid
					2401 - 获取群信息。若服务端返回groupItem，则获取群信息成功.
						groupGid
					2402 - 更改群名称。若服务端返回groupItem，则更改群名称成功.
						groupGid
						groupName
					2701 - 请求添加好友.
						verification
					2702 - 同意添加好友.
					2703 - 拒绝添加好友.
					2704 - 删除好友.
					2801 - 创建群（暂未实现）.
					2802 - 退出群.
						groupGid

			属性systemCode为0，表示发送聊天信息。此时必须定义属性content来存放聊天信息。
		*/
		this.systemCode = 0;
		}
___  
#Finish



