exports.groupItem = function () {
	this.gid = 0;
	this.name = null;
	this.member = new Array();
	this.valid = false;
	this.empty = true;
}

exports.groupItem.prototype = {
	"setGid": function (gid) {
		if (this.gid == 0) {
			if ((typeof(gid) == "number") && (gid < 0)) {
				this.gid = gid;
				return 1;
			} else {
				return 0;
			}
		} else {
			// "gid" should only be set once.
			return -1;
		}
	},
	"set": function (name) {
		//check
		if (typeof(name) != "string") {
			return 0;
		}
		//set
		this.name = name;
		//valid
		if (this.gid < 0) {
			this.valid = true;
		}
		return 1;
	},
	"searchMember": function (pid) {
		if (pid) {
			if (typeof(pid) == "number") {
				for (var i = 0; i < this.member.length; i++) {
					if (this.member[i] == pid) {
						return i;
					}
				}
				return -1;
			}
			return -2;
		}
		return -2;
	},
	"addMember": function (pid) {
		switch (this.searchMember(pid)) {
			case -1:
				// non-existent, add
				this.member.push(pid);
				if (this.empty) {
					this.empty = false;
				}
				return 1;
			case -2:
				// invalid
				return -1
			default:
				// existent
				return 0;
		}
	},
	"removeMember": function (pid) {
		var index = this.searchMember(pid);
		switch (index) {
			case -1:
				// non-existent
				return 0;
			case -2:
				// invalid
				return -1
			default:
				// existent, remove
				this.member.splice(index, 1);
				if (this.member.length == 0) {
					this.empty = true;
				}
				return 1;
		}
	}
}

exports.groupTable = {
	"content": new Array(),
	"searchIndex": function (gid) {
		if (gid) {
			if (typeof(gid) == "number") {
				for (var i = 0; i < this.content.length; i++) {
					if (this.content[i].gid === gid) {
						return i;
					}
				}
				return -1;
			}
			return -2;
		}
		return -2;
	},
	"searchObject": function (gid) {
		var index = this.searchIndex(gid);
		if (index < 0) {
			return null;
		}
		return this.content[index];
	},
	"add" : function (gi) {
		if (gi.valid !== true) {
			// groupItem invalid
			return -1;
		}
		switch (this.searchIndex(gi.gid)) {
			case -1:
				// gid non-existent, add
				this.content.push(gi);
				return 1;
			case -2:
				// gid invalid (impossible)
				return -11;
			default:
				// gid existent
				return -10;
		}
	},
	"remove" : function (gid) {
		var index = this.searchIndex(gid);
		switch (index) {
			case -1:
				// non-existent
				return 0;
			case -2:
				// invalid
				return -1;
			default:
				// existent, remove
				this.content.splice(index, 1);
				return 1;
		}
	},
	"clearEmpty" : function() {
		for (var i = 0; i < this.content.length; i++) {
			if (this.content[i].empty) {
				this.content.splice(i, 1);
				// notice
				i -= 1;
			}
		}
		return 1;
	},
	"clear" : function() {
		this.content.splice(0);
		return 1;
	}
}

exports.groupIdAllocator = {
	"counter": 0,
	"generate": function () {
		this.counter -= 1;
		return this.counter;
	}
}
