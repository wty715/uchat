exports.personItem = function () {
	this.pid = 0;
	this.loginId = null;
	this.password = null;
	this.loggedIn = false;
	this.name = null;
	this.avatar = null;
	this.friend = new Array();
	this.team = new Array();
	this.valid = false;
	this.messageQueue = new Array();
}

exports.personItem.prototype = {
	"copy": function (pi, all) {
		if (pi.valid === true) {
			this.pid = pi.pid;
			this.loginId = pi.loginId;
			this.loggedIn = pi.loggedIn;
			this.name = pi.name;
			this.avatar = pi.avatar;
			this.valid = true;
			if (all === true) {
				var i;
				for (i = 0; i < this.friend.length; i++) {
					this.friend.push(pi.friend[i]);
				}
				for (i = 0; i < this.team.length; i++) {
					this.team.push(pi.team[i]);
				}
			}
		} else {
			pi.valid = false;
		}
	},
	"setPid": function (pid) {
		if (this.pid == 0) {
			if ((typeof(pid) == "number") && (pid > 0)) {
				this.pid = pid;
				return 1;
			} else {
				return 0;
			}
		} else {
			// "pid" should only be set once.
			return -1;
		}
	},
	"setLoginId": function (loginId) {
		if (this.loginId == null) {
			if ((typeof(loginId) == "string") && (loginId != "")) {
				this.loginId = loginId;
				return 1;
			} else {
				return 0;
			}
		} else {
			// "loginId" should only be set once.
			return -1;
		}
	},
	"setPassword": function (password) {
		if ((typeof(password) == "string") && (password != "")) {
			this.password = password;
			return 1;
		} else {
			return 0;
		}
	},
	"identifyPassword": function (password) {
		if (this.password === password) {
			return true;
		} else {
			return false;
		}
	},
	"set": function (name, avatar) {
		//check
		if (typeof(name) != "string") {
			return 0;
		}
		if ((avatar != undefined) && (typeof(avatar) != "string")) {
			return 0;
		}
		//set
		this.name = name;
		if (avatar == undefined) {
			this.avatar = null;
		} else {
			this.avatar = avatar;
		}
		//valid
		if (this.pid > 0 && this.loginId != null && this.password != null) {
			this.valid = true;
		}
		return 1;
	},
	"searchFriend": function (pid) {
		if (pid) {
			if (typeof(pid) == "number") {
				for (var i = 0; i < this.friend.length; i++) {
					if (this.friend[i] == pid) {
						return i;
					}
				}
				return -1;
			}
			return -2;
		}
		return -2;
	},
	"addFriend": function (pid) {
		switch (this.searchFriend(pid)) {
			case -1:
				// non-existent, add
				this.friend.push(pid);
				return 1;
			case -2:
				// invalid
				return -1
			default:
				// existent
				return 0;
		}
	},
	"removeFriend": function (pid) {
		var index = this.searchFriend(pid);
		switch (index) {
			case -1:
				// non-existent
				return 0;
			case -2:
				// invalid
				return -1
			default:
				// existent, remove
				this.friend.splice(index, 1);
				return 1;
		}
	},
	"searchTeam": function (gid) {
		if (gid) {
			if (typeof(gid) == "number") {
				for (var i = 0; i < this.team.length; i++) {
					if (this.team[i] == gid) {
						return i;
					}
				}
				return -1;
			}
			return -2;
		}
		return -2;
	},
	"addTeam": function (gid) {
		switch (this.searchTeam(gid)) {
			case -1:
				// non-existent, add
				this.team.push(gid);
				return 1;
			case -2:
				// invalid
				return -1
			default:
				// existent
				return 0;
		}
	},
	"removeTeam": function (gid) {
		var index = this.searchTeam(gid);
		switch (index) {
			case -1:
				// non-existent
				return 0;
			case -2:
				// invalid
				return -1
			default:
				// existent, remove
				this.team.splice(index, 1);
				return 1;
		}
	},
	"enqueueMessage": function (message) {
		if (message) {
			this.messageQueue.push(message);
			return message;
		}
		return null;
	},
	"dequeueMessage": function (preread) {
		var temp;
		if (preread == true) {
			temp = this.messageQueue[0];
			if (temp == undefined) {
				return null;
			}
			return temp;
		} else {
			temp = this.messageQueue.shift();
			if (temp == undefined) {
				return null;
			}
			return temp;
		}
	}
}

exports.personTable = {
	"content": new Array(),
	"searchPid2Index": function (pid) {
		if (pid) {
			if (typeof(pid) == "number") {
				for (var i = 0; i < this.content.length; i++) {
					if (this.content[i].pid === pid) {
						return i;
					}
				}
				return -1;
			}
			return -2;
		}
		return -2;
	},
	"searchPid2Object": function (pid) {
		var index = this.searchPid2Index(pid);
		if (index < 0) {
			return null;
		}
		return this.content[index];
	},
	"searchLoginId2Index": function (loginId) {
		if (loginId) {
			if (typeof(loginId) == "string") {
				for (var i = 0; i < this.content.length; i++) {
					if (this.content[i].loginId === loginId) {
						return i;
					}
				}
				return -1;
			}
			return -2;
		}
		return -2;
	},
	"searchLoginId2Object": function (loginId) {
		var index = this.searchLoginId2Index(loginId);
		if (index < 0) {
			return null;
		}
		return this.content[index];
	},
	"add": function (pi) {
		if (pi.valid !== true) {
			// personItem invalid
			return -1;
		}
		switch (this.searchPid2Index(pi.pid)) {
			case -1:
				// pid non-existent
				switch (this.searchLoginId2Index(pi.loginId)) {
					case -1:
						// loginId non-existent
						this.content.push(pi);
						return 1;
					case -2:
						// loginId invalid (impossible)
						return -21;
					default:
						// loginId existent
						return -20;
				}
			case -2:
				// pid invalid (impossible)
				return -11;
			default:
				// pid existent
				return -10;
		}
	},
	"remove": function (pid) {
		var index = this.searchPid2Index(pid);
		switch (index) {
			case -1:
				// non-existent
				return 0;
			case -2:
				// invalid
				return -1;
			default:
				// existent, remove
				this.content.splice(index, 1);
				return 1;
		}
	},
	"clear": function() {
		this.content.splice(0);
		return 1;
	}
}

exports.personIdAllocator = {
	"counter": 0,
	"generate": function () {
		this.counter += 1;
		return this.counter;
	}
}
