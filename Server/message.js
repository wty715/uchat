exports.messageItem = function () {
	this.origin = 0;
	this.destination = 0;
	/*
		属性systemCode在非0时的含义如下：

			【由服务端发出】
				1101 - 注册成功。
				1102 - 注册失败（loginId已存在）。
				1103 - 注册失败（password为空）。
				1201 - 需要登陆。
				1202 - 登陆失败（loginId不存在）。
				1203 - 登陆失败（password错误）。
				1204 - 注销成功
				1205 - 注销失败（pid不存在）
				1301 - 修改自身密码成功。
				1302 - 修改自身密码失败（原password错误）。
				1303 - 修改自身密码失败（新password为空）。
				1304 - 获取他人信息失败（loginId不存在）。
				1305 - 获取他人信息失败（pid不存在）。
				1401 - 获取群信息失败（gid不存在）。
				1501 - 发送消息成功。
				1502 - 发送消息失败（对方非好友）。
				1503 - 发送消息失败（不在群中）。
				1601 - 消息队列为空。
				1701 - 转发成功。
				1702 - 转发失败（pid不存在）。
				1801 - 创建群失败（member为空）。
				1802 - 退出群成功。
				1803 - 退出群失败（不在群中）
				1804 - 退出群失败（gid不存在）。
				1999 - 报文格式错误。

			【客户端发出】（某些属性必须按需定义）
				2101 - 注册。
					myLoginId
					myPassword
					myName
					myAvatar
				2201 - 登陆。若服务端返回自身的personItem，则登陆成功。
					myLoginId
					myPassword
				2202 - 注销。
				2301 - 获取自身信息。
				2302 - 修改自身信息。若服务端返回自身的personItem，则修改自身信息成功。
					myName
					myAvatar
				2303 - 修改自身密码。
					myPassword
					myNewPassword
				2304 - 获取他人信息。若服务端返回他人的personItem，则获取他人信息成功。
					peerLoginId
					peerPid
				2401 - 获取群信息。若服务端返回groupItem，则获取群信息成功。
					groupGid
				2402 - 更改群名称。若服务端返回groupItem，则更改群名称成功。
					groupGid
					groupName
				2701 - 请求添加好友。
					verification
				2702 - 同意添加好友。
				2703 - 拒绝添加好友。
				2704 - 删除好友。
				2801 - 创建群（暂未实现）
				2802 - 退出群。
					groupGid

		属性systemCode为0，表示发送聊天信息。此时必须定义属性content来存放聊天信息。
	*/
	this.systemCode = 0;
}
