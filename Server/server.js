var express = require("express");
var bodyParser = require("body-parser");
var person = require("./person.js");
var group = require("./group.js");
var message = require("./message.js");

var app = express();
var personItem = person.personItem;
var personTable = person.personTable;
var personIdAllocator = person.personIdAllocator;
var groupItem = group.groupItem;
var groupTable = group.groupTable;
var messageItem = message.messageItem;
var miErr = new messageItem();
miErr.origin = 0;
miErr.destination = 0;
miErr.systemCode = 1999;

app.use(bodyParser.json());

app.get("/personTable", function (req, res) {
	res.send(personTable);
});

app.get("/groupTable", function (req, res) {
	res.send(groupTable);
});

app.get("/", function (req, res) {
	var str;
	var pid;
	var piSelf;
	var miReceive;
	str = req.get("pid");
	if (str == null) {
		res.send(miErr);
		return;
	}
	pid = parseInt(str);
	if (pid == Number.NaN) {
		res.send(miErr);
		return;
	}
	piSelf = personTable.searchPid2Object(pid);
	if (piSelf == null) {
		miReceive = new messageItem();
		miReceive.origin = 0;
		miReceive.destination = pid;
		miReceive.systemCode = 1201;
		res.send(miReceive);
	}
	if (piSelf.loggedIn == false) {
		miReceive = new messageItem();
		miReceive.origin = 0;
		miReceive.destination = pid;
		miReceive.systemCode = 1201;
		res.send(miReceive);
		return;
	}
	miReceive = piSelf.dequeueMessage();
	if (miReceive == null) {
		miReceive = new messageItem();
		miReceive.origin = 0;
		miReceive.destination = pid;
		miReceive.systemCode = 1601;
		res.send(miReceive);
		return;
	}
	res.send(miReceive);
	return;
});

app.post("/", function (req, res) {
	var piSelf;
	var piPeer;
	var miReceive;
	var miSend;
	var giReceive;
	if (req.body == null) {
		res.send(miErr);
		return;
	}
	miReceive = req.body;
	if ((miReceive.origin == null) || (miReceive.destination == null) || (miReceive.systemCode == null)) {
		res.send(miErr);
		return;
	}
	if ((typeof(miReceive.origin) != "number") || (typeof(miReceive.destination) != "number") || (typeof(miReceive.systemCode) != "number")) {
		res.send(miErr);
		return;
	}
	if (miReceive.systemCode == 2101) {
		// register
		if ((miReceive.myLoginId == null) || (miReceive.myPassword == null) || (miReceive.myName == null)) {
			res.send(miErr);
			return;
		}
		if ((typeof(miReceive.myLoginId) != "string") || (typeof(miReceive.myPassword) != "string") || (typeof(miReceive.myName) != "string")) {
			res.send(miErr);
			return;
		}
		piSelf = new personItem();
		piSelf.setPid(personIdAllocator.generate());
		piSelf.setLoginId(miReceive.myLoginId);
		if (piSelf.setPassword(miReceive.myPassword) == 0) {
			miSend = new messageItem();
			miSend.origin = 0;
			miSend.destination = miReceive.origin;
			miSend.systemCode = 1103;
			res.send(miSend);
			return;
		}
		piSelf.set(miReceive.myName, miReceive.myAvatar ? miReceive.myAvatar : null);
		if (personTable.add(piSelf) == -20) {
			miSend = new messageItem();
			miSend.origin = 0;
			miSend.destination = miReceive.origin;
			miSend.systemCode = 1102;
			res.send(miSend);
			return;
		}
		miSend = new messageItem();
		miSend.origin = 0;
		miSend.destination = miReceive.origin;
		miSend.systemCode = 1101;
		res.send(miSend);
		return;
	}
	if (miReceive.systemCode == 2201) {
		// login
		if ((miReceive.myLoginId == null) || (miReceive.myPassword == null)) {
			res.send(miErr);
			return;
		}
		if ((typeof(miReceive.myLoginId) != "string") || (typeof(miReceive.myPassword) != "string")) {
			res.send(miErr);
			return;
		}
		piSelf = personTable.searchLoginId2Object(miReceive.myLoginId)
		if (piSelf == null) {
			miSend = new messageItem();
			miSend.origin = 0;
			miSend.destination = miReceive.origin;
			miSend.systemCode = 1202;
			res.send(miSend);
			return;
		}
		if (piSelf.identifyPassword(miReceive.myPassword) == false) {
			miSend = new messageItem();
			miSend.origin = 0;
			miSend.destination = miReceive.origin;
			miSend.systemCode = 1203;
			res.send(miSend);
			return;
		}
		piSelf.loggedIn = true;
		miSend = new personItem();
		miSend.copy(piSelf, true)
		res.send(miSend);
		return;
	}
	piSelf = personTable.searchPid2Object(miReceive.origin);
	if (piSelf == null) {
		miSend = new messageItem();
		miSend.origin = 0;
		miSend.destination = miReceive.origin;
		miSend.systemCode = 1201;
		res.send(miSend);
		return;
	}
	if (piSelf.loggedIn == false) {
		miSend = new messageItem();
		miSend.origin = 0;
		miSend.destination = miReceive.origin;
		miSend.systemCode = 1201;
		res.send(miSend);
		return;
	}
	switch (miReceive.systemCode) {
		// message
		case 0:
			if (miReceive.content == null) {
				res.send(miErr);
				return;
			}
			if (typeof(miReceive.content) != "string") {
				res.send(miErr);
				return;
			}
			if (miReceive.destination > 0) {
				// to person
				if (piSelf.searchFriend(miReceive.destination) < 0) {
					miSend = new messageItem();
					miSend.origin = 0;
					miSend.destination = miReceive.origin;
					miSend.systemCode = 1502;
					res.send(miSend);
					return;
				}
				piPeer = personTable.searchPid2Object(miReceive.destination);
				piPeer.enqueueMessage(miReceive);
				// success
				miSend = new messageItem();
				miSend.origin = 0;
				miSend.destination = miReceive.origin;
				miSend.systemCode = 1501;
				res.send(miSend);
				return;
			} else if (miReceive.destination < 0) {
				// to group
				if (piSelf.searchGroup(miReceive.destination) < 0) {
					miSend = new messageItem();
					miSend.origin = 0;
					miSend.destination = miReceive.origin;
					miSend.systemCode = 1503;
					res.send(miSend);
					return;
				}
				giReceive = groupTable.searchObject(miReceive.destination);
				// to each one in group except itself
				for (var i = 0; i < giReceive.member.length; i++) {
					piPeer = personTable.searchPid2Object(giReceive.member[i])
					if (piPeer.pid == miReceive.origin) {
						continue;
					}
					piPeer.enqueueMessage(miReceive);
				}
				// success
				miSend = new messageItem();
				miSend.origin = 0;
				miSend.destination = miReceive.origin;
				miSend.systemCode = 1501;
				res.send(miSend);
				return;
			} else {
				res.send(miErr);
				return;
			}
		//system
		case 2202:
			pi = personTable.searchPid2Object(miReceive.origin);
			if (pi == null) {
				miSend = new messageItem();
				miSend.origin = 0;
				miSend.destination = miReceive.origin;
				miSend.systemCode = 1205;
				res.send(miSend);
				return;
			}
			pi.loggedIn = false;
			miSend = new messageItem();
			miSend.origin = 0;
			miSend.destination = miReceive.origin;
			miSend.systemCode = 1204;
			res.send(miSend);
			return;
		case 2301:
			miSend = new personItem();
			miSend.copy(piSelf, true)
			res.send(miSend);
			return;
		case 2302:
			if (miReceive.myName == null) {
				res.send(miErr);
				return;
			}
			if (typeof(miReceive.myName) != "string") {
				res.send(miErr);
				return;
			}
			piSelf.name = miReceive.myName;
			piSelf.avatar = miReceive.myAvatar ? miReceive.myAvatar : null;
			miSend = new personItem();
			miSend.copy(piSelf, true)
			res.send(miSend);
			return;
		case 2303:
			if ((miReceive.myPassword == null) || (miReceive.myNewPassword == null)) {
				res.send(miErr);
				return;
			}
			if ((typeof(miReceive.myPassword) != "string") || (typeof(miReceive.myNewPassword) != "string")) {
				res.send(miErr);
				return;
			}
			if (piSelf.identifyPassword(miReceive.myPassword) == false) {
				miSend = new messageItem();
				miSend.origin = 0;
				miSend.destination = miReceive.origin;
				miSend.systemCode = 1302;
				res.send(miSend);
				return;
			}
			if (piSelf.setPassword(miReceive.myNewPassword) == 0) {
				miSend = new messageItem();
				miSend.origin = 0;
				miSend.destination = miReceive.origin;
				miSend.systemCode = 1303;
				res.send(miSend);
				return;
			}
			// logout
			piSelf.loggedIn = false;
			miSend = new messageItem();
			miSend.origin = 0;
			miSend.destination = miReceive.origin;
			miSend.systemCode = 1301;
			res.send(miSend);
			return;
		case 2304:
			if (miReceive.peerLoginId && (typeof(miReceive.peerLoginId) == "string")) {
				piPeer = personTable.searchLoginId2Object(miReceive.peerLoginId);
				if (piPeer == null) {
					miSend = new messageItem();
					miSend.origin = 0;
					miSend.destination = miReceive.origin;
					miSend.systemCode = 1304;
					res.send(miSend);
					return;
				}
				miSend = new personItem();
				miSend.copy(piPeer, false)
				res.send(miSend);
				return;
			} else if (miReceive.peerPid && (typeof(miReceive.peerPid) == "number")) {
				piPeer = personTable.searchPid2Object(miReceive.peerPid);
				if (piPeer == null) {
					miSend = new messageItem();
					miSend.origin = 0;
					miSend.destination = miReceive.origin;
					miSend.systemCode = 1305;
					res.send(miSend);
					return;
				}
				miSend = new personItem();
				miSend.copy(piPeer, false)
				res.send(miSend);
				return;
			} else {
				res.send(miErr);
				return;
			}
		case 2401:
			if (miReceive.groupGid == null) {
				res.send(miErr);
				return;
			}
			if (typeof(miReceive.groupGid) != "number") {
				res.send(miErr);
				return;
			}
			giReceive = groupTable.searchObject(miReceive.groupGid);
			if (giReceive == null) {
				miSend = new messageItem();
				miSend.origin = 0;
				miSend.destination = miReceive.origin;
				miSend.systemCode = 1401;
				res.send(miSend);
				return;
			}
			miSend = new groupItem();
			miSend.copy(giReceive)
			res.send(miSend);
			return;
		case 2402:
			if ((miReceive.groupGid == null) || (miReceive.groupName == null)) {
				res.send(miErr);
				return;
			}
			if ((typeof(miReceive.groupGid) != "number") || (typeof(miReceive.groupName) != "string")) {
				res.send(miErr);
				return;
			}
			giReceive = groupTable.searchObject(miReceive.groupGid);
			if (giReceive == null) {
				miSend = new messageItem();
				miSend.origin = 0;
				miSend.destination = miReceive.origin;
				miSend.systemCode = 1401;
				res.send(miSend);
				return;
			}
			giReceive.name = miReceive.groupName;
			miSend = new groupItem();
			miSend.copy(giReceive)
			res.send(miSend);
			return;
		case 2701:
			// check
			if (miReceive.verification == null) {
				res.send(miErr);
				return;
			}
			if (typeof(miReceive.verification) != "string") {
				res.send(miErr);
				return;
			}
			piPeer = personTable.searchPid2Object(miReceive.destination);
			if (piPeer == null) {
				miSend = new messageItem();
				miSend.origin = 0;
				miSend.destination = miReceive.origin;
				miSend.systemCode = 1702;
				res.send(miSend);
				return;
			}
			// forward
			piPeer.enqueueMessage(miReceive);
			// reply
			miSend = new messageItem();
			miSend.origin = 0;
			miSend.destination = miReceive.origin;
			miSend.systemCode = 1701;
			res.send(miSend);
			return;
		case 2702:
			piPeer = personTable.searchPid2Object(miReceive.destination);
			if (piPeer == null) {
				miSend = new messageItem();
				miSend.origin = 0;
				miSend.destination = miReceive.origin;
				miSend.systemCode = 1702;
				res.send(miSend);
				return;
			}
			// add friend
			piSelf.addFriend(piPeer.pid);
			piPeer.addFriend(piSelf.pid);
			// forward
			piPeer.enqueueMessage(miReceive);
			// reply
			miSend = new messageItem();
			miSend.origin = 0;
			miSend.destination = miReceive.origin;
			miSend.systemCode = 1701;
			res.send(miSend);
			return;
		case 2703:
			piPeer = personTable.searchPid2Object(miReceive.destination);
			if (piPeer == null) {
				miSend = new messageItem();
				miSend.origin = 0;
				miSend.destination = miReceive.origin;
				miSend.systemCode = 1702;
				res.send(miSend);
				return;
			}
			// forward
			piPeer.enqueueMessage(miReceive);
			// reply
			miSend = new messageItem();
			miSend.origin = 0;
			miSend.destination = miReceive.origin;
			miSend.systemCode = 1701;
			res.send(miSend);
			return;
		case 2704:
			piPeer = personTable.searchPid2Object(miReceive.destination);
			if (piPeer == null) {
				miSend = new messageItem();
				miSend.origin = 0;
				miSend.destination = miReceive.origin;
				miSend.systemCode = 1702;
				res.send(miSend);
				return;
			}
			// remove friend
			piSelf.removeFriend(piPeer.pid);
			piPeer.removeFriend(piSelf.pid);
			// forward
			piPeer.enqueueMessage(miReceive);
			// reply
			miSend = new messageItem();
			miSend.origin = 0;
			miSend.destination = miReceive.origin;
			miSend.systemCode = 1701;
			res.send(miSend);
			return;
		case 2801:
			res.send(miErr);
			return;
		case 2802:
			if (miReceive.groupGid == null) {
				res.send(miErr);
				return;
			}
			if (typeof(miReceive.groupGid) != "number") {
				res.send(miErr);
				return;
			}
			if (piSelf.removeTeam(miReceive.groupGid) == 0) {
				miSend = new messageItem();
				miSend.origin = 0;
				miSend.destination = miReceive.origin;
				miSend.systemCode = 1803;
				res.send(miSend);
				return;
			}
			giReceive = groupTable.searchObject(miReceive.groupGid);
			if (giReceive == null) {
				miSend = new messageItem();
				miSend.origin = 0;
				miSend.destination = miReceive.origin;
				miSend.systemCode = 1804;
				res.send(miSend);
				return;
			}
			giReceive.removeMember(pi.pid);
			if (giReceive.empty) {
				groupTable.clearEmpty();
			}
			miSend = new messageItem();
			miSend.origin = 0;
			miSend.destination = miReceive.origin;
			miSend.systemCode = 1802;
			res.send(miSend);
			return;
		default:
			res.send(miErr);
			return;
	}
});

app.all("*", function (req, res) {
	res.send(miErr);
});

app.listen(8000);

setInterval(function () {
	console.log((new Date()).toLocaleString() + ", Running...");
}, 5000);
